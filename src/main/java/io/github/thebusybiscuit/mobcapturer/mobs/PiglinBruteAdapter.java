package io.github.thebusybiscuit.mobcapturer.mobs;

import org.bukkit.entity.PiglinBrute;

public class PiglinBruteAdapter extends AbstractPiglinAdapter<PiglinBrute> {

    public PiglinBruteAdapter() {
        super(PiglinBrute.class);
    }

}
